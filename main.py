import tempfile
import json
import geojson
from geojson import (
    Feature,
    FeatureCollection,
    MultiPolygon as GeoJsonMultiPolygon,
    Polygon as GeoJsonPolygon,
    LineString as GeoJsonLineString,
)
import math
from shapely.geometry import Point, LineString, Polygon, MultiPolygon, CAP_STYLE
from shapely.ops import split, substring, nearest_points
from shapely.affinity import scale, translate
import geopandas as gpd
import pandas as pd
import numpy as np
import time
from itertools import combinations
import multiprocessing as mp
from collections import namedtuple
import functools


spaces_file = "./all_spaces_plani.geojson"
access_file = "./all_access_plani.geojson"
access = None
access_index = None


def init():
    """Initialize a subprocess."""
    global access, access_index

    _start = time.time()

    access = gpd.read_file(access_file)
    access_index = access.sindex

    _end = time.time()
    print(mp.current_process(), _end - _start)


@functools.lru_cache(maxsize=None)  # add memoization to increase speed
def _get_class(fieldnames, name):
    """Create a new namedtuple class."""
    return namedtuple(name, fieldnames)


def get_namedtuple(series, name="S"):
    """Convert the series to a namedtuple."""
    my_class = _get_class(tuple(series.index), name)
    return my_class._make(series)


def process_path(my_space_tuple):
    """Match the space to its access lines."""
    my_space_index, my_space_copy = my_space_tuple
    my_space_copy["Index"] = my_space_index
    my_space = get_namedtuple(my_space_copy)
    my_ends = np.NaN
    crs = {"init": "epsg:2451"}

    # For pathways (generally complex), process
    # the exterior and interior geometries separately.
    polygons = (
        [Polygon(my_space.geometry.exterior)]
        + [Polygon(ring) for ring in my_space.geometry.interiors]
        if my_space.category in ["B028", "B029"]
        else [my_space.geometry]
    )

    for idx_poly, polygon in enumerate(polygons):
        # Find access lines connected to the space
        # (inside a buffer around the boundary).
        possible_access = access.iloc[
            list(access_index.intersection(polygon.buffer(0.015).bounds))
        ]
        possible_access = possible_access[
            (
                (possible_access["level"] == my_space.level)
                | (possible_access["level"] == my_space.to_level)
            )
            & (
                (abs(possible_access["height"] - my_space.height) < 0.01)
                | (
                    abs(possible_access["height"] - my_space.height - my_space.rise)
                    < 0.01
                )
            )
        ]
        if my_space.category in ["B022", "B024"]:
            possible_access = possible_access[
                (possible_access["level"] == my_space.level)
                & (abs(possible_access["height"] - my_space.height) < 0.01)
            ]
        precise_access = gpd.GeoDataFrame()
        if not possible_access.empty:
            precise_access = gpd.sjoin(
                possible_access,
                gpd.GeoDataFrame(geometry=[polygon.boundary.buffer(0.015)], crs=crs),
                how="inner",
                op="within",
            )
        for access_line in precise_access.itertuples():
            # Keep reference of the line
            my_ends = (
                my_ends + [access_line.Index]
                if isinstance(my_ends, list)
                else [access_line.Index]
            )

    return my_space.Index, my_ends


def get_extended_line(feature, extent):
    """Extend both end points of line with distance 'extent' (in meters)."""
    # Need to get first and last segments, get length,
    # calculate ratio so that length = length + extent,
    # scale, replace.
    first_segment = LineString(feature.coords[:2])
    first_ratio = 1 + extent / first_segment.length
    new_first_segment = scale(
        first_segment,
        xfact=first_ratio,
        yfact=first_ratio,
        origin=Point(feature.coords[1]),
    )
    last_segment = LineString(feature.coords[-2:])
    last_ratio = 1 + extent / last_segment.length
    new_last_segment = scale(
        last_segment,
        xfact=last_ratio,
        yfact=last_ratio,
        origin=Point(feature.coords[-2]),
    )
    return LineString(
        [
            new_first_segment.coords[0],
            *feature.coords[1:-1],
            new_last_segment.coords[-1],
        ]
    )


def process_stairs(my_space_tuple):
    """Create the steps as 3d polygons."""
    my_space_index, my_space_copy, my_line_copy = my_space_tuple
    my_space_copy["Index"] = my_space_index
    my_space = get_namedtuple(my_space_copy)
    my_line = get_namedtuple(my_line_copy, "L")
    crs = {"init": "epsg:2451"}

    # Create a vector between centroid and middle of end line,
    # extend it backwards, take intersection with other end.
    centroid = my_space.geometry.centroid
    projected = nearest_points(my_line.geometry, centroid)[0]
    vector = scale(
        LineString([projected, centroid]), xfact=-2, yfact=-2, origin=centroid
    )
    depth_axis = LineString(
        [projected, vector.intersection(my_space.geometry.exterior)]
    )
    total_run = depth_axis.length
    width = my_line.geometry.length

    # Standard run and rise in meters depending on type.
    # (B021: stairs, B023: escalators, B024: moving walkways, B025: slopes)
    unit_run = {
        "B021": 0.3,
        "B023": 0.35,
        "B024": total_run,  # moving walkways are special cases
        "B025": 0.1,
    }
    unit_rise = {"B021": 0.17, "B023": 0.2, "B024": 1.0, "B025": 0.05}
    # Calculate the minimum number of steps necessary
    # so that the run and rise will never be smaller than standard.
    min_nb_steps = math.floor(
        min(
            total_run / unit_run[my_space.category],
            my_space.rise / unit_rise[my_space.category],
        )
    )
    nb_steps = int(max(min_nb_steps, 1))  # there has to be 1 step minimum
    steps_runs = []

    # If long stairs (rise > 3m and excess length > 1m), add landings.
    if (
        my_space.category == "B021"
        and my_space.rise > 3
        and total_run - nb_steps * unit_run["B021"] > 1
    ):
        step_run = unit_run["B021"]  # the steps will be the standard size
        steps_runs = [step_run] * nb_steps
        # The number of landings will be the minimum out of
        # the number of times there are 2.6m in the rise,
        # and the number of meters in excess.
        min_nb_landings = math.floor(
            min(my_space.rise / 2.6, total_run - nb_steps * step_run)
        )
        # There will be at least 1 landing anyway
        nb_landings = int(max(min_nb_landings, 1))
        # How many steps before the 1st landing
        period = int(nb_steps // (nb_landings + 1))
        landing_run = (total_run - step_run * (nb_steps - nb_landings)) / nb_landings
        for i in range(1, nb_landings + 1):
            steps_runs[i * period - 1] = landing_run

    # If escalator, add a dummy 1st step, steps run no longer than standard
    # except at the ends where they can be longer.
    elif my_space.category == "B023":
        step_run = unit_run["B023"]
        steps_runs = [step_run] * nb_steps
        excess = total_run - unit_run["B023"] * nb_steps
        if excess > 0:
            steps_runs.insert(0, excess / len(my_space.ends))
            if len(my_space.ends) == 2:
                steps_runs[-1] = steps_runs[0]
        # If no excess, a dummy 1st step is added anyway,
        # and it takes half the run off the last step.
        else:
            steps_runs.insert(0, step_run / 2)
            steps_runs[-1] = steps_runs[0]

    # In all other cases, the step run will be the total run
    # divided by number of steps (might be longer than standard).
    else:
        steps_runs = [total_run / nb_steps] * nb_steps

    # The rise may get higher than standard to compensate
    # for a too small number of steps.
    step_rise = my_space.rise / nb_steps

    # Get the starting height (depending on starting from the top or the bottom,
    # and if there is a dummy 1st step).
    direction = 1 if abs(my_line.height - my_space.height) < 0.01 else -1
    step_height = (
        my_line.height
        if nb_steps == len(steps_runs) and direction == 1
        else my_line.height - step_rise
    )

    # Now with the line (extended at the ends),
    # cut the geometry along axis at each step.
    extended_line = get_extended_line(my_line.geometry, 20)
    new_geometries = []
    for i in range(len(steps_runs)):

        run = steps_runs[i]
        translation_vector = substring(depth_axis, 0, run)
        x, y = translation_vector.xy
        # Translated line (next cut).
        translated_line = translate(extended_line, xoff=x[-1] - x[0], yoff=y[-1] - y[0])
        # Line that will be at the middle of the step.
        middle_line = translate(
            extended_line, xoff=(x[-1] - x[0]) / 2, yoff=(y[-1] - y[0]) / 2
        )

        split_geoms = MultiPolygon([my_space.geometry])
        if i > 0:
            split_geoms = MultiPolygon(list(split(split_geoms, extended_line).geoms))
        if i < len(steps_runs) - 1:
            split_geoms = MultiPolygon(list(split(split_geoms, translated_line).geoms))

        step_geoms = [g for g in split_geoms.geoms if g.intersects(middle_line)]

        step_geoms_z = []
        for geom in step_geoms:
            new_exterior = [
                (x, y, round(step_height, 3)) for x, y, z in geom.exterior.coords
            ]
            new_interiors = [
                [(x, y, round(step_height, 3)) for x, y, z in ring.coords]
                for ring in geom.interiors
            ]
            new_poly = Polygon(new_exterior, holes=new_interiors)
            step_geoms_z.append(new_poly)
        new_geometries.extend(step_geoms_z)

        extended_line = translated_line
        step_height = step_height + direction * step_rise

    new_geometry = MultiPolygon(new_geometries)

    # TODO: Add side line for escalators?

    return my_space.Index, step_rise, total_run, width, new_geometry


def dump_to_geojson(gdf):
    """Take a geodataframe and return a geojson without null properties.
    
    The to_json method from (geo)pandas can not be used, 
    because it has trouble with checking nullity of list fields.
    The geometry has to be rewritten using the geojson classes, to keep
    the necessary precision (7 decimals, default is 6).
    """
    features = []
    for index, row in gdf.iterrows():
        test = []
        geoms = []
        result = np.NaN

        if row["geometry"].geom_type == "MultiPolygon":
            geoms = row["geometry"].geoms
        else:
            geoms.append(row["geometry"])
        for geom in geoms:
            test2 = []
            if geom.geom_type == "Polygon":
                test2.append(tuple(geom.exterior.coords))
                if geom.interiors:
                    test2.extend(
                        [tuple(interior.coords) for interior in geom.interiors]
                    )
            if geom.geom_type == "LineString":
                test2.append(tuple(geom.coords))
            test.append(test2)

        if row["geometry"].geom_type == "MultiPolygon":
            result = GeoJsonMultiPolygon(test, precision=7)
        else:
            test = test[0]
            if row["geometry"].geom_type == "Polygon":
                result = GeoJsonPolygon(test, precision=7)
            elif row["geometry"].geom_type == "LineString":
                test = test[0]
                result = GeoJsonLineString(test, precision=7)

        features.append(
            Feature(
                id=index,
                geometry=result,
                properties=row.dropna().drop("geometry").to_dict(),
            )
        )

    return geojson.dumps(FeatureCollection(features))


if __name__ == "__main__":

    print("Start of processing.")
    start = time.time()

    # Read spaces.
    spaces = gpd.read_file(spaces_file)
    # Convert to single polygons.
    spaces["geometry"] = [
        feature.geoms[0] if feature.geom_type == "MultiPolygon" else feature
        for feature in spaces["geometry"]
    ]
    # Add z coordinate.
    spaces["geometry"] = [
        Polygon(
            [(x, y, space.height) for x, y in space.geometry.exterior.coords],
            holes=[
                [(x, y, space.height) for x, y in ring.coords]
                for ring in space.geometry.interiors
            ],
        )
        for space in spaces.itertuples()
    ]

    # Read access lines.
    access = gpd.read_file(access_file)

    geometries = list(spaces["geometry"])
    rises = list(spaces["rise"])
    total_rises = [np.NaN] * len(spaces.index)
    lengths = [np.NaN] * len(spaces.index)
    widths = [np.NaN] * len(spaces.index)
    ends = [np.NaN] * len(spaces.index)
    sides = [np.NaN] * len(access.index)

    # Starting from there, use parallel processing.
    with mp.Pool(processes=mp.cpu_count() - 1 or 1, initializer=init) as pool:
        midstart = time.time()

        # Process all paths to match them with access lines.
        paths = spaces[
            spaces["category"].isin(
                ["B021", "B022", "B023", "B024", "B025", "B028", "B029"]
            )
        ]
        paths_ids = list(paths.index)
        paths_tuples = ((idx, spaces.iloc[idx].copy()) for idx in paths_ids)

        for path_result in pool.imap_unordered(process_path, paths_tuples):
            path_id, path_ends = path_result
            if isinstance(path_ends, list):
                ends[path_id] = path_ends
                # Add reference to space on access lines.
                for access_id in path_ends:
                    sides[access_id] = (
                        sides[access_id] + [path_id]
                        if isinstance(sides[access_id], list)
                        else [path_id]
                    )

        # Clean the end lines of false pairings.
        stairs_etc = spaces[spaces["category"].isin(["B021", "B023", "B025"])]
        for space in stairs_etc[
            (stairs_etc["rise"] > 0)
            & stairs_etc.index.map(lambda idx: isinstance(ends[idx], list))
        ].itertuples():
            # If 3 or more, check sides.
            # Keep the lines where the space is the only choice as stairs.
            # TODO: might be necessary to loop if too many stairs superposed
            if len(ends[space.Index]) > 2:
                ends[space.Index] = [
                    x
                    for x in ends[space.Index]
                    if len([y for y in sides[x] if y in stairs_etc.index]) == 1
                ]
                # Clean the sides array.
                # Keep the sides only for the current stairs/etc and pathways.
                for end in ends[space.Index]:
                    sides[end] = [
                        x
                        for x in sides[end]
                        if x not in stairs_etc.index or x == space.Index
                    ]
            # Put longest end first (for future steps reconstruction).
            ends[space.Index] = sorted(
                ends[space.Index],
                key=lambda x: access.iloc[x].geometry.length,
                reverse=True,
            )

        # Add columns.
        spaces["ends"] = ends
        access["sides"] = sides

        midend = time.time()
        print(
            "Matching of paths with access lines took {} seconds.".format(
                midend - midstart
            )
        )
        midstart = time.time()

        # From there, create the 3d geometry.

        # Only stairs/escalators/etc with 2 end lines are considered valid
        # (possibly 1 as long as rise is not null).
        valid_stairs = spaces[
            (
                spaces["category"].isin(["B021", "B023", "B025"])
                & (spaces["rise"] > 0)
                & spaces["ends"].map(lambda x: isinstance(x, list) and 1 <= len(x) <= 2)
            )
            | (
                (spaces["category"] == "B024")
                & spaces["ends"].map(lambda x: isinstance(x, list) and len(x) == 2)
            )
        ]
        valid_stairs_ids = list(valid_stairs.index)
        valid_stairs_tuples = (
            (
                idx,
                spaces.iloc[idx].copy(),
                access.iloc[spaces.iloc[idx]["ends"][0]].copy(),
            )
            for idx in valid_stairs_ids
        )

        for stairs_result in pool.imap_unordered(process_stairs, valid_stairs_tuples):
            (
                stairs_id,
                stairs_rise,
                stairs_length,
                stairs_width,
                stairs_geometry,
            ) = stairs_result
            total_rises[stairs_id] = rises[stairs_id]
            rises[stairs_id] = stairs_rise
            lengths[stairs_id] = round(stairs_length, 2)
            widths[stairs_id] = round(stairs_width, 2)
            geometries[stairs_id] = stairs_geometry

        # Update or add columns
        spaces["rise"] = rises
        spaces["total_rise"] = total_rises
        spaces["length"] = lengths
        spaces["width"] = widths
        spaces["geometry"] = geometries

        midend = time.time()
        print(
            "Creation of 3D steps for stairs/etc took {} seconds.".format(
                midend - midstart
            )
        )

    # TODO: Clean sides of the access line of stairs leading to nowhere?

    # TODO: clean sides of elevators with no rise?

    # TODO: clean ends that reference sides that have been cleaned?

    # Drop the access lines not matched.
    access = access[access["sides"].notna()]

    # Change coordinate system to geo (WGS84).
    spaces = spaces.to_crs(epsg=4326)
    access = access.to_crs(epsg=4326)

    # Separate the map features and network data.
    spaces_map = spaces[["category", "level", "inner", "height", "rise", "geometry"]]
    spaces_net = pd.DataFrame(
        spaces[
            spaces["category"].isin(
                ["B021", "B022", "B023", "B024", "B025", "B028", "B029"]
            )
            & spaces["ends"].notna()
            & (
                (~spaces["category"].isin(["B021", "B023", "B024", "B025"]))
                | (spaces["ends"].map(lambda x: isinstance(x, list) and len(x) == 2))
            )
            & (
                (~spaces["category"].isin(["B021", "B023", "B025"]))
                | (spaces["rise"] > 0)
            )
        ][["category", "inner", "height", "ends", "geometry"]]
    )
    spaces_bug = spaces[
        spaces["to_level"].notna()
        & (
            (
                spaces["category"].isin(["B021", "B023", "B024", "B025"])
                & spaces["ends"].map(lambda x: isinstance(x, list) and len(x) != 2)
            )
            | (
                (spaces["category"] == "B022")
                & (spaces["ends"].map(lambda x: isinstance(x, list) and len(x) != 1))
            )
        )
    ]

    # Then drop null values and convert to (geo)json
    spaces_map_json = dump_to_geojson(spaces_map)
    spaces_net_json = dump_to_geojson(spaces_net)
    access_json = dump_to_geojson(access)

    # Write to files
    tmp_file = tempfile.mkstemp(suffix="_spaces.json", dir="./")
    with open(tmp_file[1], "w") as outfile:
        outfile.write(spaces_map_json)
    tmp_file = tempfile.mkstemp(suffix="_network.json", dir="./")
    with open(tmp_file[1], "w") as outfile:
        outfile.write(spaces_net_json)
    tmp_file = tempfile.mkstemp(suffix="_access.json", dir="./")
    with open(tmp_file[1], "w") as outfile:
        outfile.write(access_json)

    if not spaces_bug.empty:
        spaces_bug_json = dump_to_geojson(spaces_bug)
        tmp_file = tempfile.mkstemp(suffix="_bugs.json", dir="./")
        with open(tmp_file[1], "w") as outfile:
            outfile.write(spaces_bug_json)

    end = time.time()
    print("Complete processing took {} seconds.".format(end - start))
