from h3.h3 import *
import geojson
import tempfile


with open('./emprisec.geojson') as f:
    geoJsons = geojson.load(f)

hexagons = [list(polyfill(geoJson['geometry'], 15, True)) for geoJson in geoJsons['features']]

centroids = [geojson.Feature(geometry=geojson.Point(tuple(reversed(h3_to_geo(hexRef)))), properties={"h3": hexRef})
             for hexList in hexagons for hexRef in hexList]

# polylines = []
# allpoly = []
# for hexa in hexagons:
#     polygons = h3.h3_set_to_multi_polygon([hexa], geo_json=True)
#     allpoly.append(geojson.Feature(geometry={"type": "Polygon", "coordinates": polygons[0]}, properties={}))

tmp_file = tempfile.mkstemp(suffix='.geojson', dir='./')
with open(tmp_file[1], 'w') as outfile:
    geojson.dump(geojson.FeatureCollection(centroids), outfile)
